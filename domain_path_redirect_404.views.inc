<?php

/**
 * @file
 * Provide views data for domain_path_redirect_404.module.
 */

/**
 * Implements hook_views_data().
 */
function domain_path_redirect_404_views_data() {
  $data = [];

  // Only define views data if the service uses our specific implementation.
  if (!\Drupal::service('domain_path_redirect_404.storage') instanceof \Drupal\domain_path_redirect_404\DomainLoggerStorage) {
    return $data;
  }

  $data['domain_path_redirect_404']['table']['group'] = t('Domain Logger log');

  $data['domain_path_redirect_404']['table']['base'] = [
    'field' => '',
    'title' => t('Domain Logger log'),
    'help' => t('Overview for 404 and 403 error paths per domain.'),
  ];

  $data['domain_path_redirect_404']['path'] = [
    'title' => t('Path'),
    'help' => t('The path of the request.'),
    'field' => [
      'id' => 'standard',
    ],
    'filter' => [
      'id' => 'string',
    ],
  ];

  $data['domain_path_redirect_404']['langcode'] = [
    'title' => t('Language'),
    'help' => t('The language of this request.'),
    'field' => [
      'id' => 'domain_path_redirect_404_langcode',
    ],
    'filter' => [
      'id' => 'language',
    ],
  ];

  $data['domain_path_redirect_404']['domain'] = [
    'title' => t('Domain'),
    'help' => t('The domain of the request.'),
    'field' => [
      'id' => 'standard',
    ],
    'filter' => [
      'id' => 'domain',
    ],
  ];

  $data['domain_path_redirect_404']['type'] = [
    'title' => t('Type'),
    'help' => t('The code of request logged.'),
    'field' => [
      'id' => 'numeric',
    ],
    'filter' => [
      'id' => 'numeric',
    ],
  ];

  $data['domain_path_redirect_404']['count'] = [
    'title' => t('Count'),
    'help' => t('The number of requests with that path and language.'),
    'field' => [
      'id' => 'numeric',
      'click sortable' => TRUE,
    ],
    'filter' => [
      'id' => 'numeric',
    ],
  ];

  $data['domain_path_redirect_404']['daily_count'] = [
    'title' => t('Daily count'),
    'help' => t('The number of requests with that path and language in a day.'),
    'field' => [
      'id' => 'numeric',
      'click sortable' => TRUE,
    ],
    'filter' => [
      'id' => 'numeric',
    ],
  ];

  $data['domain_path_redirect_404']['timestamp'] = [
    'title' => t('Timestamp'),
    'help' => t('The timestamp of the last request with that path and language.'),
    'field' => [
      'id' => 'date',
      'click sortable' => TRUE,
    ],
    'filter' => [
      'id' => 'date',
    ],
  ];

  $data['domain_path_redirect_404']['resolved'] = [
    'title' => t('Resolved'),
    'help' => t('Whether or not this path has a redirect assigned.'),
    'field' => [
      'id' => 'boolean',
    ],
    'filter' => [
      'id' => 'boolean',
      'label' => t('Resolved'),
      'use_equal' => TRUE,
    ],
  ];

  return $data;
}
