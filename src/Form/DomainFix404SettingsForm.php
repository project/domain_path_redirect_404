<?php

namespace Drupal\domain_path_redirect_404\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

class DomainFix404SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'domain_path_redirect_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['domain_path_redirect_404.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('domain_path_redirect_404.settings');
    $row_limits = [100, 1000, 10000, 100000, 1000000];
    $form['row_limit'] = [
      '#type' => 'select',
      '#title' => t('404 error database logs to keep'),
      '#default_value' => $config->get('row_limit'),
      '#options' => [0 => t('All')] + array_combine($row_limits, $row_limits),
      '#description' => t('The maximum number of error logs to keep in the database log. Requires a <a href=":cron">cron maintenance task</a>.',
        [':cron' => Url::fromRoute('system.status')->toString()]),
    ];

    $ignored_pages = $config->get('pages');
    // Add a new path to be ignored, if there is an ignore argument in the query.
    if ($path_to_ignore = \Drupal::request()->query->get('ignore')) {
      $ignored_pages .= "\n" . $path_to_ignore;
    }

    $form['ignore_pages'] = [
      '#type' => 'textarea',
      '#title' => t('Pages to ignore'),
      '#default_value' => $ignored_pages,
      '#description' => t("Specify pages by using their paths. Enter one path per line. The '*' character is a wildcard. An example path is %user-wildcard for every user page. %front is the front page.", [
        '%user-wildcard' => '/user/*',
        '%front' => '<front>',
      ]),
    ];

    $form['suppress_404'] = [
      '#type' => 'checkbox',
      '#title' => t("Suppress 'page not found' log messages"),
      '#default_value' => $config->get('suppress_404'),
      '#description' => t("Prevents logging 'page not found' events. Can be safely enabled when domain_path_redirect_404 module is used, which stores them separately, nothing else relies on those messages."),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Make sure to store the 'pages to ignore' with the leading slash.
    $ignore_pages = explode(PHP_EOL, $form_state->getValue('ignore_pages'));
    $pages = '';
    foreach ($ignore_pages as $page) {
      if (!empty($page)) {
        $pages .= '/' . ltrim($page, '/') . "\n";
      }
    }

    \Drupal::configFactory()
      ->getEditable('domain_path_redirect_404.settings')
      ->set('row_limit', $form_state->getValue('row_limit'))
      ->set('pages', $pages)
      ->set('suppress_404', $form_state->getValue('suppress_404'))
      ->save();
    parent::submitForm($form, $form_state);
  }

}
