<?php

namespace Drupal\domain_path_redirect_404\Form;

use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Url;
use Drupal\domain_path_redirect_404\DomainLoggerStorageInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a form that lists all 404 error paths and no redirect assigned yet.
 *
 * This is a fallback for the provided default view.
 */
class DomainLoggerLogForm extends FormBase {

  /**
   * The language manager.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * The redirect storage.
   *
   * @var DomainLoggerStorageInterface
   */
  protected $domainLoggerStorage;

  /**
   * The date formatter service.
   *
   * @var \Drupal\Core\Datetime\DateFormatterInterface
   */
  protected $dateFormatter;

  /**
   * The entity manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs a RedirectFix404Form.
   *
   * @param \Drupal\Core\Language\LanguageManagerInterface $language_manager
   *   The language manager.
   * @param DomainLoggerStorageInterface $domain_path_redirect_404_storage
   *   The redirect storage.
   * @param \Drupal\Core\Datetime\DateFormatterInterface $date_formatter
   *   The date Formatter service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity manager.
   */
  public function __construct(LanguageManagerInterface $language_manager, DomainLoggerStorageInterface $domain_path_redirect_404_storage, DateFormatterInterface $date_formatter, EntityTypeManagerInterface $entity_type_manager) {
    $this->languageManager = $language_manager;
    $this->domainLoggerStorage = $domain_path_redirect_404_storage;
    $this->dateFormatter = $date_formatter;
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('language_manager'),
      $container->get('domain_path_redirect_404.storage'),
      $container->get('date.formatter'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'domain_path_redirect_404_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $destination = $this->getDestinationArray();

    $domains = $this->entityTypeManager->getStorage('domain')->loadMultiple();

    foreach ($domains as $key => $domain) {
      $domains[$key] = $domain->label();
    }

    array_unshift($domains, $this->t('- All -'));

    $request = $this->getRequest();
    $search = $request->get('search');
    $domain = $request->get('domain');
    $type = $request->get('type');

    $form['#attributes'] = ['class' => ['search-form']];

    $form['basic'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Filter'),
      '#attributes' => ['class' => ['container-inline']],
    ];
    $form['basic']['filter'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Path'),
      '#default_value' => $search,
      '#maxlength' => 128,
      '#size' => 25,
    ];
    $form['basic']['domain'] = [
      '#type' => 'select',
      '#title' => $this->t('Domain'),
      '#default_value' => $domain,
      '#options' => $domains,
    ];
    $form['basic']['type'] = [
      '#type' => 'select',
      '#title' => $this->t('Type'),
      '#default_value' => $type,
      '#options' => [
        $this->t('- All - '),
        403 => 403,
        404 => 404,
      ],
    ];
    $form['basic']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Filter'),
      '#action' => 'filter',
    ];
    if ($search) {
      $form['basic']['reset'] = [
        '#type' => 'submit',
        '#value' => $this->t('Reset'),
        '#action' => 'reset',
      ];
    }

    $languages = $this->languageManager->getLanguages(LanguageInterface::STATE_ALL);
    $multilingual = $this->languageManager->isMultilingual();

    $header = [
      ['data' => $this->t('Path'), 'field' => 'source'],
      ['data' => $this->t('Domain'), 'field' => 'domain'],
      ['data' => $this->t('Type'), 'field' => 'type'],
      ['data' => $this->t('Count'), 'field' => 'count', 'sort' => 'desc'],
      ['data' => $this->t('Daily count'), 'field' => 'daily_count'],
      ['data' => $this->t('Last accessed'), 'field' => 'timestamp'],
    ];
    if ($multilingual) {
      $header[] = ['data' => $this->t('Language'), 'field' => 'language'];
    }
    $header[] = ['data' => $this->t('Resolved'), 'field' => 'resolved'];
    $header[] = ['data' => $this->t('Operations')];

    $rows = [];
    $results = $this->domainLoggerStorage->listRequests($header, $search, $domain, $type);
    foreach ($results as $result) {
      $path = ltrim($result->path, '/');

      $row = [];
      $row['source'] = $path;
      $row['domain'] = $domains[$result->domain];
      $row['type'] = $result->type;
      $row['count'] = $result->count;

      if (!isset($result->daily_count)) {
        if ($result->daily_min == $result->daily_max) {
          $row['daily_count'] = $result->daily_min;
        }
        else {
          $row['daily_count'] = $result->daily_min . " - " . $result->daily_max;
        }
      }
      else {
        $row['daily_count'] = $result->daily_count;
      }

      $row['timestamp'] = $this->dateFormatter->format($result->timestamp, 'short');
      if ($multilingual) {
        if (isset($languages[$result->langcode])) {
          $row['language'] = $languages[$result->langcode]->getName();
        }
        else {
          $row['language'] = $this->t('Undefined @langcode', ['@langcode' => $result->langcode]);
        }
      }
      $row['resolved'] = isset($result->resolved) ? $result->resolved : 'N/A';

      $operations = [];
      if ($this->entityTypeManager->getAccessControlHandler('redirect')
        ->createAccess()) {
        if ($result->type == 404) {
          $operations['add'] = [
            'title' => $this->t('Add redirect'),
            'url' => Url::fromRoute('domain_path_redirect.add', [],
              [
                'query' => [
                    'source' => $path,
                    'language' => $result->langcode,
                    'domain' => $result->domain,
                  ] + $destination,
              ]),
          ];
        }

        $operations['ignore'] = [
          'title' => $this->t('Ignore'),
          'url' => Url::fromRoute('domain_path_redirect_404.ignore_404', [], [
            'query' => [
                'path' => $path,
                'language' => $result->langcode,
              ] + $destination,
          ]),
        ];
      }
      $row['operations'] = [
        'data' => [
          '#type' => 'operations',
          '#links' => $operations,
        ],
      ];

      $rows[] = $row;
    }

    $form['domain_path_redirect_404_table'] = [
      '#theme' => 'table',
      '#header' => $header,
      '#rows' => $rows,
      '#empty' => $type != NULL ? $this->t('There are no @type logs.', ['@type' => $type])
        : $this->t('There are no 403 or 404 logs.'),
    ];

    $form['domain_path_redirect_404_pager'] = ['#type' => 'pager'];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    if ($form_state->getTriggeringElement()['#action'] == 'filter') {
      $form_state->setRedirect('domain_path_redirect_404.fix_404', [],
        [
          'query' =>
            [
              'search' => trim($form_state->getValue('filter')),
              'domain' => $form_state->getValue('domain'),
              'type' => $form_state->getValue('type'),
            ],
        ]
      );
    }
    else {
      $form_state->setRedirect('domain_path_redirect_404.fix_404');
    }
  }

}
