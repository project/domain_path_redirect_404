<?php

namespace Drupal\domain_path_redirect_404\EventSubscriber;

use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\domain\DomainNegotiatorInterface;
use Drupal\domain_path_redirect_404\DomainLoggerStorageInterface;
use Drupal\Core\Path\CurrentPathStack;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * An EventSubscriber that listens to errors.
 */
class DomainLoggerSubscriber implements EventSubscriberInterface {

  /**
   * The current path.
   *
   * @var \Drupal\Core\Path\CurrentPathStack
   */
  protected $currentPath;

  /**
   * The request stack (get the URL argument(s) and combined it with the path).
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * The language manager.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * The redirect storage.
   *
   * @var \Drupal\domain_path_redirect_404\DomainLoggerStorageInterface
   */
  protected $loggerStorage;

  /**
   * @var \Drupal\domain\DomainInterface
   */
  protected $domain;

  /**
   * Constructs a new Redirect404Subscriber.
   *
   * @param \Drupal\Core\Path\CurrentPathStack $current_path
   *   The current path.
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   The request stack.
   * @param \Drupal\Core\Language\LanguageManagerInterface $language_manager
   *   The language manager.
   * @param \Drupal\domain_path_redirect_404\DomainLoggerStorageInterface $loggerStorage
   *   A redirect storage.
   */
  public function __construct(CurrentPathStack $current_path, RequestStack $request_stack, LanguageManagerInterface $language_manager, DomainLoggerStorageInterface $loggerStorage, DomainNegotiatorInterface $domainNegotiator) {
    $this->currentPath = $current_path;
    $this->requestStack = $request_stack;
    $this->languageManager = $language_manager;
    $this->loggerStorage = $loggerStorage;
    $this->domain = $domainNegotiator->getActiveDomain();
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events[KernelEvents::EXCEPTION][] = 'onKernelException';
    return $events;
  }

  /**
   * Logs an exception of 404 and 403 errors.
   *
   * @param GetResponseForExceptionEvent $event
   *   Is given by the event dispatcher.
   */
  public function onKernelException(GetResponseForExceptionEvent $event) {
    $exception = $event->getException();
    // Only log page not found (404) or access denies (403) errors.
    if ($exception instanceof NotFoundHttpException || $exception instanceof AccessDeniedHttpException) {
      $path = $this->currentPath->getPath();

      // Allow to store paths with arguments.
      if ($query_string = $this->requestStack->getCurrentRequest()->getQueryString()) {
        $query_string = '?' . $query_string;
      }
      $path .= $query_string;
      $langcode = $this->languageManager->getCurrentLanguage()->getId();

      // Write record.
      $this->loggerStorage->logRequest($path, $langcode, $this->domain->id(), $exception->getStatusCode());
    }
  }

}
