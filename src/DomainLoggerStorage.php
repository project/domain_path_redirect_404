<?php


namespace Drupal\domain_path_redirect_404;


use Drupal\Component\Utility\Unicode;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Database\Connection;

class DomainLoggerStorage implements DomainLoggerStorageInterface {


  const MAX_PATH_LENGTH = 191;

  /**
   * @var \Drupal\Core\Database\Connection $database
   */
  protected $database;

  /**
   * @var \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   */
  protected $configFactory;

  public function __construct(Connection $database, ConfigFactoryInterface $configFactory) {
    $this->database = $database;
    $this->configFactory = $configFactory;
  }

  public function logRequest($path, $langcode, $domain, $type) {
    if (mb_strlen($path) > static::MAX_PATH_LENGTH) {
      // Don't attempt to log paths that would result in an exception. There is
      // no point in logging truncated paths, as they cannot be used to build a
      // new redirect.
      return;
    }
    // Ignore invalid UTF-8, which can't be logged.
    if (!Unicode::validateUtf8($path)) {
      return;
    }

    // If the request is not new, update its count and timestamp.
    $this->database->merge('domain_path_redirect_404')
      ->key('path', $path)
      ->key('langcode', $langcode)
      ->key('domain', $domain)
      ->key('type', $type)
      ->expression('count', 'count + 1')
      ->expression('daily_count', 'daily_count + 1')
      ->fields([
        'timestamp' => \Drupal::time()->getRequestTime(),
        'count' => 1,
        'daily_count' => 1,
        'resolved' => 0,
      ])
      ->execute();
  }

  /**
   * {@inheritdoc}
   */
  public function listRequests(array $header = [], $search = NULL, $domain = NULL, $type = NULL) {
    /** @var \Drupal\Core\Database\Query\SelectInterface $query */
    $query = $this->database
      ->select('domain_path_redirect_404', 'dll')
      ->extend('Drupal\Core\Database\Query\TableSortExtender')
      ->orderByHeader($header)
      ->extend('Drupal\Core\Database\Query\PagerSelectExtender')
      ->limit(25);

    if ($search) {
      // Replace wildcards with PDO wildcards.
      // @todo Find a way to write a nicer pattern.
      $wildcard = '%' . trim(preg_replace('!\*+!', '%', $this->database->escapeLike($search)), '%') . '%';
      $query->condition('path', $wildcard, 'LIKE');
    }
    if ($domain) {
      $query->condition('domain', $domain);
    }
    $query->fields('dll');
    if ($type) {
      $query->condition('type', $type);
    }
    return $query->condition('resolved', 0, '=')->execute()->fetchAll();
  }

  // TODO: Fix, for some reason not working
  public function resolveLogRequest($path, $langcode, $domain) {
    $this->database->update('domain_path_redirect_404')
      ->fields(['resolved' => 1])
      ->condition('path', $path)
      ->condition('langcode', $langcode)
      ->condition('domain', $domain)
      ->condition('type', 404)
      ->execute();
  }

  public function purgeOldRequests() {
    $row_limit = $this->configFactory->get('domain_path_redirect_404.settings')
      ->get('row_limit');

    // In admin form 0 used as value for 'All' label.
    if ($row_limit == 0) {
      return;
    }

    $query = $this->database->select('domain_path_redirect_404', 'r404');
    $query->fields('r404', ['timestamp']);
    // On databases known to support log(), use it to calculate a logarithmic
    // scale of the count, to delete records with count of 1-9 first, then
    // 10-99 and so on.
    if ($this->database->driver() == 'mysql' || $this->database->driver() == 'pgsql') {
      $query->addExpression('floor(log(10, count))', 'count_log');
      $query->orderBy('count_log', 'DESC');
    }
    $query->orderBy('timestamp', 'DESC');
    $cutoff = $query
      ->range($row_limit, 1)
      ->execute()
      ->fetchAssoc();

    if (!empty($cutoff)) {
      // Delete records having older timestamp and less visits (on a logarithmic
      // scale) than cutoff.
      $delete_query = $this->database->delete('domain_path_redirect_404');

      if ($this->database->driver() == 'mysql' || $this->database->driver() == 'pgsql') {
        // Delete rows with same count_log AND older timestamp than cutoff.
        $and_condition = $delete_query->andConditionGroup()
          ->where('floor(log(10, count)) = :count_log2', [':count_log2' => $cutoff['count_log']])
          ->condition('timestamp', $cutoff['timestamp'], '<=');

        // And delete all the rows with count_log less than the cutoff.
        $condition = $delete_query->orConditionGroup()
          ->where('floor(log(10, count)) < :count_log1', [':count_log1' => $cutoff['count_log']])
          ->condition($and_condition);
        $delete_query->condition($condition);
      }
      else {
        $delete_query->condition('timestamp', $cutoff['timestamp'], '<=');
      }
      $delete_query->execute();
    }
  }

  public function resetDailyCount() {
    $this->database->update('domain_path_redirect_404')
      ->fields(['daily_count' => 0])
      ->execute();
  }

}
