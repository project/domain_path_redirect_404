<?php


namespace Drupal\domain_path_redirect_404;


interface DomainLoggerStorageInterface {

  /**
   * Merges a request log in the database.
   *
   * @param string $path
   *   The path of the current request.
   * @param string $langcode
   *   The ID of the language code.
   * @param string $domain
   *   The ID of current domain
   * @param string $type
   *   The type of the error
   */
  public function logRequest($path, $langcode, $domain, $type);

  /**
   * Marks a 404 request log as resolved.
   *
   * @param string $path
   *   The path of the current request.
   * @param string $langcode
   *   The ID of the language code.
   * @param int $type
   *   The type of error
   */
  public function resolveLogRequest($path, $langcode, $type);

  /**
   * Returns the 404 request data.
   *
   * @param array $header
   *   An array containing arrays of the redirect_404 fields data.
   * @param string $search
   *   The search text. It is possible to have multiple '*' as a wildcard.
   * @param string $domain
   *   The domain id to filter results
   * @param int $type
   *   The error code to filter results
   *
   * @return array
   *   A list of objects with the properties:
   *   - path
   *   - type
   *   - count
   *   - timestamp
   *   - langcode
   *   - resolved
   */
  public function listRequests(array $header = [], $search = NULL, $domain = NULL, $type = NULL);

  /**
   * Cleans the irrelevant 404 and 403 request logs.
   */
  public function purgeOldRequests();

  /**
   * Resets the daily counts of 404 request logs.
   */
  public function resetDailyCount();

}
